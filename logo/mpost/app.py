from flask import Flask, Response, request
from werkzeug.exceptions import InternalServerError
import tempfile
import os.path
import subprocess
import shutil
from sys import argv
import re

app = Flask(__name__)

def getmp (width, height, spacing, spacingvertical):
  return """
    prologues := 3;
    outputtemplate := "loop-%c.svg";
    outputformat := "svg";

    beginfig(1)

      h:={height}mm;
      w:={width}mm;
      s:={spacing}mm;
      t:={spacingvertical}mm;
      lby:=0.5;
      lbx:=0.1;

      z1=(0w,0h);
      z2=((1-lbx)*w,lby*h);
      z3=(0.5w,1h);
      z4=(lbx*w,lby*h);
      z5=(w+.5(s-w),0h);

      pickup pencircle scaled 4;

      draw z5 shifted(-s, -t){{right}}...z2...z3...z4...{{right}}z5 ... z2 shifted (s,t) ... z3 shifted (s,t) ... z4  shifted (s,t) ... {{right}}z5 shifted (s,t) ... z2 shifted (2s,2t) ... z3 shifted (2s,2t) ... z4 shifted (2s,2t) ... {{right}}z5 shifted (2s,2t);

      % dotlabels.top(1, 2, 3, 4);

    endfig;

    end;""".format(width=width, height=height, spacing=spacing, spacingvertical=spacingvertical)

def getsvg (width, height, spacing, spacingvertical):
  tmpdir = tempfile.mkdtemp()

  with open(os.path.join(tmpdir, 'loop.mp'), 'w') as mpfile:
    mp = getmp(width, height, spacing, spacingvertical)
    # print(mp)
    mpfile.write(mp)
    mpfile.close()
    
    log = subprocess.check_output(['mpost', 'loop.mp'], cwd=tmpdir, stderr=subprocess.STDOUT)
    with open(os.path.join(tmpdir, 'loop-1.svg'), 'r') as svgfile:
      svg = svgfile.read()
      shutil.rmtree(tmpdir)
      return svg

  return None

@app.route('/svg/generate', methods=["POST"])
def gensvg ():
  mp = """
    prologues := 3;
    outputtemplate := "generated.svg";
    outputformat := "svg";
    {}
    end;""".format(request.form['mp'])

  tmpdir = tempfile.mkdtemp()

  with open(os.path.join(tmpdir, 'generated.mp'), 'w') as mpfile:
    mpfile.write(mp)
    mpfile.close()
    
    try:
      log = subprocess.check_output(['mpost', '-halt-on-error', 'generated.mp'], cwd=tmpdir, stderr=subprocess.STDOUT).decode()
      with open(os.path.join(tmpdir, 'generated.svg'), 'r') as svgfile:
        svg = svgfile.read()
        # shutil.rmtree(tmpdir)
        return Response(svg, mimetype='image/svg+xml')
    except subprocess.CalledProcessError as e:
        return InternalServerError(e.output.decode())

@app.route('/svg/<int:width>/<int:height>/<int:spacing>/<int:spacingvertical>', methods=["GET"])
def svg(width, height, spacing, spacingvertical):
  txt = ' '.join(['CONTOUR 9 ― COLTAN AS COTTON ― ' for i in range(100)])
  code = getsvg(width, height, spacing, spacingvertical)
  code = re.sub(r'style=".+"', 'style="fill:none;stroke:none;"', code).replace('<path', '<path id="shape" stroke="none"').replace('</svg>', '<text style="font-size: 24px; letter-spacing: 5px"><textPath href="#shape">{}</textPath></text></svg>'.format(txt))

  return Response(code, mimetype='image/svg+xml')

@app.after_request
def add_headers(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')

    return response

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5556, debug=True)
