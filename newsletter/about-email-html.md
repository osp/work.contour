# Notes on making an html email

**First thing first : this is a shitty work to do, but I found it quite interesting in fact (I was still angry and disappointed at some points)**

I only did my tests on Gmail and Outlook, so things might be different with other clients

I say that because :
  - you can't expect to use full CCS3 possibilities
  - you can't expect to have your own fonts on every devices (even with `<links>`, `@import`, `@font-face` or `base64`)
  - you can't expect your design to work exactly the same on all plateforms (hardware & softwares)
  - it's better to design html email with `<table></table>` to avoid design changes
  - your CSS should be inline, but...
  - CSS online inliners are not trust worthy (as you will see)
  - If your *one* of your style property in an element is not good for him, Gmail robot will strip *all* styling for the current element. This extreme behaviour is not observed in Outlook
  - you will always need to have fallback solution for your styling

### Why using table ?
  Apparently it's the best way to have a design that stays well in multiple platforms. Knowing also that floats, transforms, absolute/relative positionning doesn't work well everywhere, `<table>` are the safest way to control how your email will look like, even if it is not the prettiest coding method in 2018...

#### Notes on tables
  - margins will not work everywhere, use paddings instead, but this is not a fully confirmed behaviour.
  - you can nest a child `<table>` inside the `<td>` of a parent `<table>` if you want to play it simple for specific layout (like two columns with differents sized lines), or to have centered content.
  - You can have `<td>` with regular `<div>` inside like you do for regular html pages

### Some strange things that happened to me

#### Svg & Png story
Gmail did not wanted .svg type images (Outlook had no problem with them). I had to use .png instead. But the thing is .png with transparency rendered blurry for some reason, putting a background color solved the problem. But I had some problem with simple white background, for some reason... Turns out that it might also be that my file wasn't big enough in resolution, but that's not sure either.
#### True black
For a strange reason, the text of my email where transformed from regular black to greyish color, on both Outlook & Gmail. `#000000;`, `#000;`, `rgb(0,0,0);` didn't work. The problem was solved by simply putting `color:'black';` manually on each `<div>` with text in it. Leaving empty font color to have the default black didn't work either.
#### The `color: #000001;` secret
Gmail tends to override the color of links if you set them with a `color:#000000;` / `"black"`. You can use a slightly not full black to avoid that, like `#000001;`. Possibly compatible with previous point.
#### Inliners not Gmail compatible
Online inliners like [campaign monitor](https://inliner.cm/) and [Mailchimp](https://templates.mailchimp.com/resources/inline-css/) one's do a half good job, you'll need to correct things by hand. For example when declaring a font this way : `font-family 'MyFont';` it will be inlined `style="font-family:"MyFont";"` So you have double quotes inside double quotes, And Gmail will remove everything. You should then, by hand change that to `style="font-family: 'Myfont';"`
#### Gmail is sensitive
You should *always* put spaces after ";" "," and ":". So from this : `"font-family:'Arial','Calibri';color:#000001;"` you should make it like that : `"font-family: 'Arial', 'Calibri'; color: #000001;"`
#### Gmail is *really* sensitive
You should *never* have spaces inside quotes. When declaring fonts, things like `'My Font'` will be stripped out by Gmail (and all styling with it, remember), but things like `'My-Font'` or `'MyFont'` work well.
