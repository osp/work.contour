# Transform SVG to PNG
PREVIOUS=""
for FILE in svg/*.svg
do
  FILENAME=$(basename $FILE)
  echo "PREVIOUS: ${PREVIOUS} FILE: ${FILENAME}"
  DIFF=$(cmp "svg/${PREVIOUS}" "svg/${FILENAME}")
  if [ "$PREVIOUS" != "" ] && [ "$DIFF" = "" ]
  then
    echo "Copy frame ${FILENAME}"
    cp "png/${PREVIOUS%.*}.png" "png/${FILENAME%.*}.png"
  else
    inkscape -z -e png/${FILENAME%.*}.png -w 1080 -h 1920 $FILE;
    PREVIOUS=$FILENAME
  fi
done

ffmpeg -i png/%05d.png -c:v libx264 -vf fps=25 -pix_fmt yuv420p 1080x1920.mp4;